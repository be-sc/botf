# -*- coding: utf-8 -*-

import string
from collections import namedtuple

# Changing the pits per row and/or the seeds count *should* work. Changing the row count
# *will* fail because the program is not generic enough. Also I’m not sure if the game rules
# keep making sense for anything but 2 rows per player.
BOARD_ROW_COUNT = 4
BOARD_PITS_PER_ROW = 8
BOARD_SEEDS_COUNT = BOARD_ROW_COUNT * BOARD_PITS_PER_ROW * 2

AlgoResult = namedtuple('AlgoResult', 'pit_id captured_seeds board')
INVALID_PIT = None


class InternalBoard:
    """
    The data structure used by the processing algorithms. Contains two lists of pits, for
    player and opponent. See Doku.rst for further details.

    WARNING: Assumes a row count of 4!
    """
    def __init__(self, raw_board, pits_per_row):
        self._pits_per_row = pits_per_row
        self._opponents_near_row = list(raw_board[0])

        self.player = raw_board[-1][::-1] + raw_board[-2]
        self.opponent = ([0] * self._pits_per_row) + raw_board[1]

    def to_raw_board(self):
        """ Returns the raw board corresponding to this InternalBoard. """
        return [
              list(self._opponents_near_row)
            , self.opponent[self._pits_per_row:]
            , self.player[self._pits_per_row:]
            , self.player[:self._pits_per_row][::-1]
        ]

    def pit_id(self, pit):
        """
        Returns the player pit ID corresponding to the given pit index.
        """
        pit += 1
        return '{}{:0>2}'.format(('B' if pit > self._pits_per_row else 'A'), pit)

    def __getitem__(self, pit):
        return self.player[pit]

    def __setitem__(self, pit, seeds):
        self.player[pit] = seeds

    def __len__(self):
        return len(self.player)


def has_movable_pit(player_or_opponent):
    """
    Takes the player or opponent lists from an InternalBoard. Returns True if there is at least
    one playable pit (i.e. a pit with 2 or more seeds), False otherwise.
    :type player_or_opponent: list[int]
    :rtype: bool
    """
    for pit in player_or_opponent:
        if pit > 1:
            return True

    return False


def print_board(raw_or_internal_board, indent):
    """
    Prints a raw board or internal baord to stdout in the same layout
    as shown in the documentation.
    """
    if isinstance(raw_or_internal_board, InternalBoard):
        board = raw_or_internal_board.to_raw_board()
    elif isinstance(raw_or_internal_board, list):
        board = raw_or_internal_board
    else:
        raise TypeError('Given board has wrong type.\n'
                        'Expected: InternalBoard or list[list[int]]\n'
                        'Actual: %s' % type(raw_or_internal_board).__name__)

    row_ids, pit_ids = build_pit_ids(BOARD_PITS_PER_ROW)

    for index, row in enumerate(board):
        print('{} {} | {} |'.format(
              ' '*indent
            , row_ids[index]
            , ' '.join('{: >2}'.format(pit) for pit in row)
        ), end='')

        if index == 0:
            print(' opponent')
        elif index == len(board)-1:
            print(' player')
        elif index == (len(board) // 2 - 1):
            print('\n{}---+-{}+----------'.format(' '*indent, '-'*(len(row)*3)))
        else:
            print('')  # only needed for the line break


def build_pit_ids(pits_per_row):
    """
    Builds the human readable row and pit IDs as shown in the documentation.
    Returns a tuple containing the list of row IDs and a matrix of pit IDs.
    WARNING: Assumes a row count of 4!
    :rtype: (list[str], list[list[str]])
    """
    row_count = 4
    row_ids = string.ascii_uppercase[:row_count][::-1]
    pit_indexes = range(1, pits_per_row * (row_count // 2) + 1)

    return row_ids, [
          ['{}{:0>2}'.format(row_ids[0], fidx) for fidx in pit_indexes[:pits_per_row]]
        , ['{}{:0>2}'.format(row_ids[1], fidx) for fidx in pit_indexes[pits_per_row:][::-1]]
        , ['{}{:0>2}'.format(row_ids[2], fidx) for fidx in pit_indexes[pits_per_row:]]
        , ['{}{:0>2}'.format(row_ids[3], fidx) for fidx in pit_indexes[:pits_per_row][::-1]]
    ]


