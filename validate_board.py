# -*- coding: utf-8 -*-

import csv
import sys
import bao

def main(args):
    if not args:
        print("""
Usage:
    validate_board <input filepath>
""")
    csv_filepath = args[0]
    print('Input file:', csv_filepath)

    csv_data_list = bao.preprocess_input_csv(csv_filepath)

    if not csv_data_list:
        print("\nInput file does not contain any boards.")
        return

    for csv_index, csv_data in enumerate(csv_data_list, start=1):
        print('\n----- Board no.', csv_index, '-----')
        print('Status: ', end='')


        try:
            raw_board = [row for row in csv.reader(csv_data, delimiter=';')]
            raw_board = bao.validated(raw_board, csv_filepath, err_msg_with_filepath=False)
        except ValueError as err:
            print('invalid')
            print('Reason:', str(err))
        else:
            print('valid')
            print('Movable: ', end='')

            can_move, msg = bao.check_movability(raw_board, is_pre_turn=True)

            if can_move:
                print('yes')
            else:
                print('no')
                print('Reason:', msg)


if __name__ == '__main__':
    main(sys.argv[1:])
