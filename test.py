#/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Note that the whole test suite assumes the 4×8 board layout.
"""
from multiprocessing import active_children

import os
import os.path as osp
import unittest
import algo_bruteforce
import bao
import utils

SCRIPT_DIRPATH = osp.dirname(__file__)
if not osp.isabs(SCRIPT_DIRPATH):
    SCRIPT_DIRPATH = osp.join(os.getcwd(), SCRIPT_DIRPATH)


class BoardLoadingTest(unittest.TestCase):
    def test_multiboards_load_ok(self):
        expected_boardlist = [
            ['1;4;4;0;0;0;0;0',
             '0;3;1;1;1;1;1;13',
             '8;6;4;0;0;0;0;0',
             '2;0;0;2;2;2;0;8']
            ,
            ['2;5;4;5;1;1;1;1',
             '2;0;0;0;0;0;0;10',
             '0;8;4;6;0;0;0;0',
             '0;3;3;1;1;1;1;4']
            ,
            ['0;2;2;5;4;1;1;1',
             '2;1;1;1;1;1;1;7',
             '5;5;3;1;1;1;1;2',
             '2;2;0;1;0;0;0;9']
            ,
            ['1;4;5;4;3;2;2;2',
             '1;1;1;0;0;0;0;6',
             '3;1;1;1;1;1;2;2',
             '2;0;3;4;1;1;0;9']
        ]
        self.assertEqual(expected_boardlist,
                         bao.preprocess_input_csv(osp.join(SCRIPT_DIRPATH, 'test_multiboards.csv')))

    def test_build_pit_ids(self):
        row_ids, pit_ids = utils.build_pit_ids(8)

        self.assertEqual('DCBA', row_ids)
        self.assertEqual(
            [
                ['D01', 'D02', 'D03', 'D04', 'D05', 'D06', 'D07', 'D08'],
                ['C16', 'C15', 'C14', 'C13', 'C12', 'C11', 'C10', 'C09'],
                ['B09', 'B10', 'B11', 'B12', 'B13', 'B14', 'B15', 'B16'],
                ['A08', 'A07', 'A06', 'A05', 'A04', 'A03', 'A02', 'A01']
            ],
            pit_ids
        )

    def test_valid_boards_validate_ok(self):
        board_at_game_start_before_validation = [
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','2','2','2','2']
        ]
        board_at_game_start_after_validation = [
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2]
        ]
        self.assertEqual(board_at_game_start_after_validation,
                         bao.validated(board_at_game_start_before_validation, 'dummy.csv'))

        board_after_1_move_before_validation = [
            ['2','2','2','2','2','2','2','2'],
            ['0','2','2','2','2','0','2','2'],
            ['0','3','3','3','3','0','3','3'],
            ['3','3','0','3','3','1','4','1']
        ]
        board_after_1_move_after_validation = [
            [2,2,2,2,2,2,2,2],
            [0,2,2,2,2,0,2,2],
            [0,3,3,3,3,0,3,3],
            [3,3,0,3,3,1,4,1]
        ]
        self.assertEqual(board_after_1_move_after_validation,
                         bao.validated(board_after_1_move_before_validation, 'dummy.csv'))

    def test_board_with_too_many_rows_doesnt_validate(self):
        board = [
            ['2','2','2','2','2','2','2','2'],
            ['2','0','2','2','2','2','0','2'],
            ['4','1','3','3','3','3','0','3'],
            ['1','0','3','3','0','3','3','3'],
            ['4','1','3','3','3','3','0','3']
        ]
        self.assertRaisesRegex(
            ValueError, r'Wrong number of rows',
            bao.validated, board, 'dummy.csv'
        )

    def test_board_with_too_many_pits_doesnt_validate(self):
        board = [
            ['2','2','2','2','2','2','2','2'],
            ['2','0','2','2','2','2','0','1','1'],
            ['4','1','3','3','3','3','0','3'],
            ['1','0','3','3','0','3','3','3']
        ]
        self.assertRaisesRegex(
            ValueError, r'Wrong number of pits in row C',
            bao.validated, board, 'dummy.csv'
        )

    def test_board_with_nonum_seed_value_doesnt_validate(self):
        board = [
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','2','2','spam','2'],
            ['2','2','2','2','2','2','2','2']
        ]
        self.assertRaisesRegex(
            ValueError, r'Wrong value for the number of seeds in pit B15',
            bao.validated, board, 'dummy.csv'
        )

    def test_board_with_negative_seed_value_doesnt_validate(self):
        board = [
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','2','2','2','2'],
            ['2','-5','2','2','2','2','2','2']
        ]
        self.assertRaisesRegex(
            ValueError, r'Wrong value for the number of seeds in pit A07',
            bao.validated, board, 'dummy.csv'
        )

    def test_board_with_too_little_seeds_doesnt_validate(self):
        board = [
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','2','2','2','2'],
            ['2','2','2','2','0','2','2','2'],
            ['2','2','2','2','2','2','2','2']
        ]
        self.assertRaisesRegex(
            ValueError, r'Wrong number of seeds on the board',
            bao.validated, board, 'dummy.csv'
        )


class MovabilityTest(unittest.TestCase):
    def setUp(self):
        self.playable1 = [
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2]
        ]
        self.playable2 = [
           [2,2,2,2,2,2,2,2],
           [0,2,2,2,2,0,2,2],
           [0,3,3,3,3,0,3,3],
           [3,3,0,3,3,1,4,1]
        ]
        self.rowB_empty = [
            [2,2,2,2,2,2,2,2],
            [0,5,5,5,5,0,5,5],
            [0,0,0,0,0,0,0,0],
            [3,3,0,3,3,1,4,1]
        ]
        self.rowC_empty = [
            [2,2,2,2,2,2,2,2],
            [0,0,0,0,0,0,0,0],
            [0,1,6,6,2,7,1,6],
            [4,1,4,0,1,4,4,1]
        ]
        self.rowBC_empty = [
            [2,3,8,8,4,9,3,8],
            [0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0],
            [4,1,4,0,1,4,4,1]
        ]
        self.player_nomove = [
            [3,3,3,3,3,3,3,3],
            [3,3,3,3,3,2,2,2],
            [1,0,1,1,1,1,1,1],
            [1,1,1,0,1,1,0,1]
        ]
        self.opponent_nomove = [
            [1,0,1,1,1,1,1,1],
            [1,1,1,0,1,1,0,1],
            [3,3,3,3,3,3,3,3],
            [3,3,3,3,3,2,2,2]
        ]

    def test_playable_preturn_board_is_recognized(self):
        self.assertTrue(bao.check_movability(self.playable1, is_pre_turn=True)[0])
        self.assertTrue(bao.check_movability(self.playable2, is_pre_turn=True)[0])

    def test_winner_detected_for_preturn_empty_row(self):
        self.assertIn('Opponent won', bao.check_movability(self.rowB_empty, is_pre_turn=True)[1])
        self.assertIn('Player won',   bao.check_movability(self.rowC_empty, is_pre_turn=True)[1])
        self.assertIn('Opponent won', bao.check_movability(self.rowBC_empty, is_pre_turn=True)[1])

    def test_winner_detected_for_preturn_nomove(self):
        self.assertIn('Opponent won', bao.check_movability(self.player_nomove, is_pre_turn=True)[1])
        self.assertTrue(bao.check_movability(self.opponent_nomove, is_pre_turn=True)[0])

    def test_playable_postturn_board_is_recognized(self):
        self.assertTrue(bao.check_movability(self.playable1, is_pre_turn=False)[0])
        self.assertTrue(bao.check_movability(self.playable2, is_pre_turn=False)[0])

    def test_winner_detected_for_postturn_empty_row(self):
        self.assertIn('Opponent won', bao.check_movability(self.rowB_empty, is_pre_turn=False)[1])
        self.assertIn('Player won',   bao.check_movability(self.rowC_empty, is_pre_turn=False)[1])
        self.assertIn('Player won',   bao.check_movability(self.rowBC_empty, is_pre_turn=False)[1])

    def test_winner_detected_for_postturn_nomove(self):
        self.assertTrue(bao.check_movability(self.player_nomove, is_pre_turn=False)[0])
        self.assertIn('Player won', bao.check_movability(self.opponent_nomove, is_pre_turn=False)[1])


class InternalBoardTest(unittest.TestCase):
    def setUp(self):
        self.raw_board_before = [
            [ 1, 2, 3, 4, 5, 6, 7, 8],
            [ 9,10,11,12,13,14,15,16],
            [51,52,53,54,55,56,57,58],
            [41,42,43,44,45,46,47,48]
        ]
        self.iboard = utils.InternalBoard(self.raw_board_before, 8)

    def test_converts_fromto_raw_board_correctly(self):
        self.assertEqual([48,47,46,45,44,43,42,41,51,52,53,54,55,56,57,58], self.iboard.player)
        self.assertEqual([0,0,0,0,0,0,0,0,9,10,11,12,13,14,15,16], self.iboard.opponent)
        self.assertEqual(self.raw_board_before, self.iboard.to_raw_board())

    def test_index_converts_to_correct_pit_id(self):
        self.assertEqual('A03', self.iboard.pit_id(2))
        self.assertEqual('A08', self.iboard.pit_id(7))
        self.assertEqual('B09', self.iboard.pit_id(8))
        self.assertEqual('B15', self.iboard.pit_id(14))


class AlgoBruteforceTest(unittest.TestCase):
    def test_start_of_game(self):
        start_board = utils.InternalBoard([
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2],
            [2,2,2,2,2,2,2,2],
        ], 8)
        expected_result = utils.AlgoResult(pit_id='B13', captured_seeds=8, board=[
            [2,2,2,2,2,2,2,2],
            [0,0,2,2,0,2,0,2],
            [0,1,4,4,2,5,1,4],
            [4,1,4,0,1,4,4,1],
        ])

        actual_result = algo_bruteforce.algo_bruteforce(start_board)

        self.assertEqual(expected_result.pit_id, actual_result.pit_id)
        self.assertEqual(expected_result.captured_seeds, actual_result.captured_seeds)
        self.assertEqual(expected_result.board, actual_result.board.to_raw_board())

    def test_two_movable_pits(self):
        start_board = utils.InternalBoard([
            [3,3,3,4,3,3,3,3],
            [3,3,3,3,3,4,3,3],
            [0,0,0,0,1,1,2,1],
            [1,1,1,1,2,1,1,0]
        ], 8)
        expected_result = utils.AlgoResult(pit_id='B15', captured_seeds=3, board=[
            [3,3,3,4,3,3,3,3],
            [3,3,3,3,3,4,0,3],
            [1,0,0,0,1,1,0,2],
            [2,0,2,2,0,2,2,1]
        ])

        actual_result = algo_bruteforce.algo_bruteforce(start_board)

        self.assertEqual(expected_result.pit_id, actual_result.pit_id)
        self.assertEqual(expected_result.captured_seeds, actual_result.captured_seeds)
        self.assertEqual(expected_result.board, actual_result.board.to_raw_board())


if __name__ == '__main__':
    unittest.main()
