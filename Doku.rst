Bao Optimal Turn Finder (BOTF)
==============================

Dieses Programm ist ein Beitrag von `Brother John <https://ngb.to/members/1517-Brother-John>`_ zur `Dritten ngb.to-Programmieraufgabe <https://ngb.to/threads/22781-Programmieraufgabe-(3)-Bao-Das-Steinchenspiel>`_.

Lizenz: MIT (siehe Datei *LICENCE*)


.. contents:: Inhalt


Installation und Benutzung
~~~~~~~~~~~~~~~~~~~~~~~~~~

Voraussetzungen
---------------

* `Python 3 <https://python.org/>`_. Entwickelt habe ich es mit 3.5.1, zumindest 3.4 sollte aber auch funktionieren. Python 2 könnte mit kleineren Anpassungen auch klappen. Wenn jemand Lust hat, zu kompatibilisieren …
* Keine zusätzlichen Pakete nötig.
* Getestet nur unter Windows. Ich sehe aber nicht, warum es unter Linux nicht unverändert laufen sollte.
* Optional: `restview <https://pypi.python.org/pypi/restview>`_, um diese Doku in hübsch und bequem im Browser anzuschauen.


Programmaufruf
--------------

Je nachdem, wie euer System konfiguriert ist, klappen einer oder mehrere dieser Aufrufe::

    python3 bao.py <Quell-CSV> [-a <Algorithmus-Name>]
    python bao.py <Quell-CSV> [-a <Algorithmus-Name>]
    bao.py <Quell-CSV> [-a <Algorithmus-Name>]
    bao <Quell-CSV> [-a <Algorithmus-Name>]

Eine Quell-CSV-Datei muss angegeben werden. Der Algorithmus ist optional. Standardmäßig wird der »bruteforce« verwendet. Das ist im Moment auch der einzig vorhandene. ;-) Mehr zu den Algorithmen: siehe weiter unten.

Es gibt noch ein paar Optionen mehr. Für die volle Liste::

    bao --help


Programmablauf
--------------

1. Einlesen und Validieren der Quell-CSV. Ergebnis ist ein ``raw_board``.

2. Erzeugen einer ``InternalBoard``-Instanz aus dem ``raw_board``. ``InternalBoard`` ist so aufbereitet, dass Algorithmen möglichst bequem damit arbeiten können.

3. Aufruf des Algorithmus, der die Brettkonfiguration am Rundenende, die Anzahl der gefangenen Körner und die optimale Startkuhle zurückliefert. Details zum Algorithmus-Interface: siehe unten.

4. Ausgabe von Ziel-CSV, Gefangenenzahl und Startkuhlen-ID.


Dateiübersicht
--------------

* ``bao.py``: Hauptprogramm. Enthält den ganzen administrativen Code:
    * Kommandozeilenparsing
    * CSV-Handling
    * Spielbrett-Validierung
    * Bildschirmausgabe
* ``utils.py``: Datenstrukturen & Co. für die Algorithmen.
* ``algo_*.py``: Hierdrin steckt die eigentliche Spiel-Logik.
* ``test.py``: Naja … die Testuite halt …


Spielbrett
~~~~~~~~~~

Das Spielbrett ist in Reihen (rows) und Kuhlen (pits) unterteilt, die folgendermaßen nummeriert sind::

    D | 01 02 03 04 05 06 07 08 | Gegner
    C | 16 15 14 13 12 11 10 09 |
    --+-------------------------+--------
    B | 09 10 11 12 13 14 15 16 |
    A | 08 07 06 05 04 03 02 01 | Spieler

Kuhlen werden durch ihre Reihennummer gefolgt von der Kuhlennummer identifiziert, z.B. *A05*. In den Kuhlen liegen Körner (seeds) – die »Spielsteine«.


Validierung und Movability
~~~~~~~~~~~~~~~~~~~~~~~~~~

Die **Validierung** ist eher langweilig. Sie kümmert sich darum festzustellen, ob überhaupt ein gültiges Brett geladen wurde. Ist die Reihen- und Kuhlenanzahl konsistent? Stimmt die Anzahl der Körner auf dem Spielbrett? Etc. Zugehörige Funktion: ``validate()``.

Mit **Movability** bezeichne ich, ob auf einem erfolgreich validierten Spielbrett ein Zug möglich ist. Ein Zug ist nur möglich, wenn:

* mindestens eine Spielerkuhle mehr als 1 Korn enthält und

* weder Reihe B noch Reihe C komplett leer sind, denn dann hat entweder Spieler oder Gegner gewonnen. Züge nach Spielende sind *undefined behaviour*, und das darf natürlich nicht sein.

Zugehörige Funktion: ``check_movability()``. Interessantes Detail: Die Prüfung unterscheidet sich je nachdem, ob das Brett mit der Ausgangs-Spiellage oder das Brett nach der gespielten Runde geprüft wird. Der Knackpunkt ist der Grenzfall, wenn **beide Frontreihen (B und C) leer sind**. Klar ist: das Spiel ist beendet. Bleibt die Frage: Wer hat gewonnen?

Die Situation ist extrem selten. Sie kann nur auftreten, wenn am Anfang einer Runde schon fast alle Kuhlen der Reihen B und C leer sind. Eine Komplettleerung kann in folgenden Situationen auftreten:

* *Szenario 1*: Kuhlen B16 und C09 sind als einziges noch gefüllt. *Spieler* ist am Zug. Er zieht, leert damit beide Reihen und hat gewonnen.

* *Szenario 2*: Kuhlen B09 und C16 sind als einziges noch gefüllt. *Gegner* ist am Zug. Er zieht, leert damit beide Reihen und hat gewonnen.

Bei Doppel-Leerreihen als Ausgangslage hat Gegner gewonnen, da er als letztes am Zug war. Es kann also nur Szenario 2 infrage kommen. Bei Doppel-Leerreihen als Ziellage kommt umgekehrt nur Szenario 1 infrage. Spieler hat gewonnen.

Für den Movability-Check wird deshalb für Ausgangslagen zuerst Reihe B auf Leerung gecheckt, dann Reihe C; bei Ziellagen umgekehrt.


Datenstrukturen
~~~~~~~~~~~~~~~

Format der Eingabedaten
-----------------------

Im Quellcode wird ein Spielbrett in der oben beschriebenen Form als *raw board* bezeichnet und als zweidimendionale Integer-Matrix (Liste von Listen) abgebildet. Oder in `Type-Annotation-Form <https://www.jetbrains.com/help/pycharm/2016.1/type-hinting-in-pycharm.html#legacy>`_: ``list[list[int]]``. Raw-Boards werden zum Lesen/Schreiben der Quell-/Ziel-CSV-Dateien verwendet.

Die Quelldatei mit der Ausgangsstellung ist eine CSV-Datei entspr. dem Raw-Board-Layout, d.h. sie hat 4 Zeilen mit jeweils 8 Kuhlen, in denen jeweils die Anzahl der enthaltenen Körner (ein Integer >= 0) steht. The Gesamtzahl der Körner muss 64 betragen, weil am Start des Spiels in jeder Kuhle 2 Körner liegen und nie Körner vom Spielbrett genommen werden.

Einige dieser Werte können im Quellcode angepasst werden. Siehe den Anfang der ``utils.py`` für Details.


Interne Datenstruktur für die Algorithmen
-----------------------------------------

Ein Raw-Board ist zu unbequem, um darauf herumzualgorithmieren. Deswegen gibt es die Klasse ``InternalBoard``. Die enthält zwei Listen (``list[int]``), je eine für Spieler und Gegner.

* Die Listen sind beide gleich lang. Sie enthalten jeweils alle 16 Kuhlen.

* In der Spieler-Liste sind die Kuhlen einfach in aufsteigender Reihenfolge sortiert.

* In der Gegner-Liste sind die ersten 8 Kuhlen (Reihe D) leer (also enthalten Wert ``0``), da die dortige Anzahl der Körner für die gespielte Runde irrelevant sind.

* Die zwei Hälfte der Gegner-Liste enthält die Kuhlen C16–C09 in absteigender Reihenfolge.

Klingt auf Anhieb wirr, hat aber Vorteile.

* Die Listen-Indexe von Reihe C und B sind identisch. Wenn also ein Zug z.B. in Kuhle B12 startet (entspricht Listenindex 11) und ich dafür die Körner aus der Gegnerkuhle C13 dazunehmen muss, kann ich die Gesamtanzahl einfach als ``player[11] + opponent[11]`` berechnen. Kein nerviges Index-Umrechnen nötig.

* Ich will mich genausowenig darum kümmern müssen, ob ich gegnerische Körner dazunehmen muss oder nicht. Deswegen das Nullen der ersten Hälfte der Gegnerliste. Auf Spielerseite entsprechen dieselben Indexe der Reihe A. Fängt ein Zug z.B. auf A03 (Listenindex 2) an, rechne ich genauso wie oben ``player[2] + opponent[2]``. Da der Index bei Gegner genullt ist, führt das trotzdem zur richtigen Körnerzahl.


Algorithmus-Interface
~~~~~~~~~~~~~~~~~~~~~

Da es mehrere Möglichkeiten gibt, das Problem zu lösen, habe ich ein allgemeines Interface definiert, hinter das man beliebige Algorithmen hängen kann. Welcher Algorithmus verwendet wird, kann beim Programmaufruf mit der Option ``-a`` angegeben werden.

Jeder Algorithmus ist ein eigenes Python-Modul (eine ``.py``-Datei) benannt nach dem Schema ``algo_<Algorithmus-Name>``. Der Ausführer (Puh, klingt das Scheiße im Deutschen; im Vergleich zu *executor*) innerhalb des Moduls muss genauso heißen.

Beispiel: Der *Bruteforce*-Algorithmus steht in der Datei ``algo_bruteforce.py``. Darin gibt es eine Ausführungsfunktion namens ``algo_bruteforce()``.

Der Ausführer muss ein `Callable <https://docs.python.org/3/library/functions.html#callable>`_ (z.B. eine Funktion) mit folgender Signatur sein: ``(InternalBoard) -> (str, int, InternalBoard)``. Das heißt, sie nimmt ein InternalBoard-Objekt als einzigen Parameter und gibt ein Tuple aus drei Elementen zurück; in dieser Reihenfolge:

* ID der optimalen Startkuhle,
* Gesamtanzahl der gefangenen gegnerischen Körner,
* InternalBoard-Objekt mit der Brettkonfiguration am Rundenende.

Das zurückgelieferte InternalBoard-Objekt muss keine neue Instanz sein, sondern darf auch das in-place veränderte ursprüngliche Objekt sein. Was sinnvoller ist, muss der Algorithmus selbst entscheiden.

Ein Algorithmus darf davon ausgehen, dass er ein spielbares Brett übergeben bekommt.


Algorithmen
~~~~~~~~~~~

Bruteforce
----------

**Kommandozeilen-Kürzel:** ``bruteforce``

**Motto:** Moore’s Law For The Win!

Es gibt lediglich 16 mögliche Lösungen, entsprechend der 16 möglichen Startkuhlen. *Bruteforce* probiert beginnend mit A01 der Reihe nach alle durch, merkt sich für jede das Ergebnis und liefert dann die Variante mit der höchsten Gefangenenzahl zurück.

*Bruteforce* eignet sich gut als Referenzalgorithmus, weil er den kompletten Lösungsraum abgrast. Er findet also unter Garantie die richtige Lösung.
