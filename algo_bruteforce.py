# -*- coding: utf-8 -*-

import copy
import utils


def algo_bruteforce(initial_board):
    candidate = utils.AlgoResult(pit_id=utils.INVALID_PIT, captured_seeds=-1, board=initial_board)

    print('\nAttempt | start | moves | captured seeds')
    print(  '--------+-------+-------+---------------')

    for start_pit in range(0, len(initial_board)):
        board = copy.deepcopy(initial_board)
        captured_seeds, moves = play_turn(start_pit, board)

        pit_id = board.pit_id(start_pit)
        print('{:>7} | {:<5} | {:<5} | {}'.format(start_pit, pit_id, moves, captured_seeds))

        # If necessary update the candidate pit for most seeds captured.
        if captured_seeds > candidate.captured_seeds:
            candidate = utils.AlgoResult(pit_id, captured_seeds, board)

    return candidate


def play_turn(from_pit, board):
    """
    Plays a turn starting from the given pit and returns the sum of captured seeds.
    Modifies board, i.e. after the function returns *board* reflects the state of the game
    at the end of the turn.

    :type board: utils.InternalBoard
    """
    captured_seeds = 0
    moves = 0

    # Starting pit of a move must have at least 2 seeds.
    while board[from_pit] > 1:
        # Pick up seeds from the starting pit and possibly the adjacent opponent’s pit.
        captured_seeds += board.opponent[from_pit]
        flying_seeds = board[from_pit] + board.opponent[from_pit]
        board[from_pit] = 0
        board.opponent[from_pit] = 0

        # Deposit 1 seed each to the following pits until no more seeds are flying.
        while flying_seeds > 0:
            from_pit += 1
            if from_pit == len(board):
                from_pit = 0

            board[from_pit] += 1
            flying_seeds -= 1

        moves += 1

        # Game over conditions:
        # - player cannot move anymore
        # - opponent’s front line is empty
        # - player’s front line is empty
        if ((not utils.has_movable_pit(board.player))
                or (sum(board.opponent) == 0)
                or (sum(board.player[len(board)//2:]) == 0)):
            break

    return captured_seeds, moves
