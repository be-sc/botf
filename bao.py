#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import csv
import importlib
import sys
import utils

VERSION = '0.2'

INDENT = 4


def main():
    print("Brother John's Bao Optimal Turn Finder (BOTF), version {}\n".format(VERSION))

    if ((utils.BOARD_ROW_COUNT % 2) != 0) or ((utils.BOARD_PITS_PER_ROW % 2) != 0):
        raise RuntimeError('Both BOARD_ROW_COUNT and BOARD_PITS_PER_ROW must be even numbers.')

    if utils.BOARD_SEEDS_COUNT <= 0:
        raise RuntimeError('The game is not playable with BOARD_SEEDS_COUNT <= 0.')

    args = parse_cli()

    try:
        # load input file and check all preconditions. Note that input files may contain
        # multiple boards
        csv_data_list = preprocess_input_csv(args.csv_filepath)

        if not csv_data_list:
            raise IOError("Input file '%s' does not contain any boards." % args.csv_filepath)

    except Exception as err:
        if args.is_debug:
            raise
        print('\nA {} occurred.\n'.format(type(err).__name__) + str(err))
        return False

    all_succeeded = True
    real_csv_filepath = args.csv_filepath
    real_outcsv_filepath = args.outcsv_filepath

    for csv_index, csv_data in enumerate(csv_data_list, start=1):
        try:
            if csv_index > 1:
                print('\n' + '<>'*39)

            if len(csv_data_list) > 1:
                args.csv_filepath = '{}#{}'.format(real_csv_filepath, csv_index)
                if real_outcsv_filepath:
                    args.outcsv_filepath = '{}#{}'.format(real_outcsv_filepath, csv_index)

            start_internal_board, start_raw_board = load_board(csv_data, args.csv_filepath)

            print_start_layout(start_raw_board, args)

            can_move, msg = check_movability(start_raw_board, is_pre_turn=True)
            if not can_move:
                print('Cannot run the algorithm. No moves are possible on this board.')
                print(msg)
                return True

            # run algorithm
            algo_func = load_algo(args.algo)
            result = algo_func(start_internal_board)

            print_result_layout(result)

            can_move, msg = check_movability(result.board.to_raw_board(), is_pre_turn=False)
            if not can_move:
                print(msg)

            # if needed, write the output CSV file
            if args.outcsv_filepath:
                with open(args.outcsv_filepath, 'w', newline='') as out_fp:
                    writer = csv.writer(out_fp, delimiter=';')

                    for row in result.board.to_raw_board():
                        writer.writerow(row)

        except Exception as err:
            all_succeeded = False
            if args.is_debug:
                raise
            print('\nA {} occurred.\n'.format(type(err).__name__) + str(err))

    return all_succeeded


def preprocess_input_csv(csv_filepath):
    """
    Reads the input CSV file from disk. Supports multiboard files, i.e. files that contain
    multiple board definitions separated by at least one empty line each.
    Returns a list of CSV board data (one list item per board). Items can be fed directly
    to Pythons inbuilt CSV parser.
    :rtype: list[list[str]]
    """
    input_boards = []
    curr_board = []

    with open(csv_filepath, 'r') as csv_fp:
        for line in csv_fp:
            line = line.strip()

            if line:
                curr_board.append(line)
            else:
                if curr_board:
                    input_boards.append(curr_board)
                    curr_board = []

        if curr_board:
            input_boards.append(curr_board)

    return input_boards


def load_board(csv_iterable, csv_filepath):
    """
    Loads the starting board from the given CSV file, validates it and returns a tuple
    containing the internal board and the raw board. Raises ValueError if the validation fails.
    :rtype: (InternalBoard, list[list[int]])
    """
    raw_board = [row for row in csv.reader(csv_iterable, delimiter=';')]
    raw_board = validated(raw_board, csv_filepath)
    return utils.InternalBoard(raw_board, utils.BOARD_PITS_PER_ROW), raw_board


def validated(raw_board, csv_filepath, err_msg_with_filepath=True):
    """
    Validates the given raw board and returns a new raw board with integer seed counts.
    Raises ValueError if the validation fails.
    :rtype: list[list[int]]
    """
    if err_msg_with_filepath:
        filepath_fmt = 'Input file: {csv}\n'
    else:
        filepath_fmt = ''

    row_ids, pit_ids = utils.build_pit_ids(utils.BOARD_PITS_PER_ROW)

    if len(raw_board) != utils.BOARD_ROW_COUNT:
        raise ValueError((filepath_fmt + 'Wrong number of rows.\nExpected: {exp}\nActual: {act}')
                         .format(csv=csv_filepath, exp=utils.BOARD_ROW_COUNT, act=len(raw_board)))

    validated_raw_board = []
    seeds_sum = 0

    for row_index, row in enumerate(raw_board):
        if len(row) != utils.BOARD_PITS_PER_ROW:
            raise ValueError((filepath_fmt + 'Wrong number of pits in row {row}.\n'
                                            'Expected: {exp}\nActual: {act}')
                             .format(csv=csv_filepath, row=row_ids[row_index],
                                     exp=utils.BOARD_PITS_PER_ROW, act=len(row)))

        numberized_row = []

        for pit_index, pit in enumerate(row):
            try:
                seed_count = int(pit)
                if seed_count < 0:
                    raise ValueError

                seeds_sum += seed_count
            except ValueError:
                if not pit:
                    pit = 'no value'
                raise ValueError((filepath_fmt + 'Wrong value for the number of seeds in pit {pit}.\n'
                                 'Expected: a number >= 0\nActual: {act}')
                                 .format(csv=csv_filepath, pit=pit_ids[row_index][pit_index], act=pit))

            numberized_row.append(seed_count)

        validated_raw_board.append(numberized_row)

    if seeds_sum != utils.BOARD_SEEDS_COUNT:
        raise ValueError((filepath_fmt + 'Wrong number of seeds on the board.\n'
                         'Expected: {exp}\nActual: {act}')
                         .format(csv=csv_filepath, exp=utils.BOARD_SEEDS_COUNT, act=seeds_sum))

    return validated_raw_board


def check_movability(raw_board, is_pre_turn):
    """
    Checks, if player can move on the given board.
    :param is_pre_turn: Check must happen differently depending on whether checking the
        input board layout or the layout after the player's turn. See the documentation
        for more details.
    :rtype: (bool, str)
    """
    def has_movable_pit(rows):
        for row in rows:
            for pit in row:
                if pit > 1:
                    return True

    # Yeah, yeah. I know. Code duplication. I was lazy, but what’cha gonna do about it?
    if is_pre_turn:
        if sum(raw_board[2]) == 0:
            return False, 'Game over. Opponent won.'

        if sum(raw_board[1]) == 0:
            return False, 'Game over. Player won.'

        if not has_movable_pit(raw_board[-2:]):
            return False, "Game over. None of player's pits has at least 2 seeds. Opponent won."

    else:
        if sum(raw_board[1]) == 0:
            return False, 'Game over. Player won.'

        if sum(raw_board[2]) == 0:
                return False, 'Game over. Opponent won.'

        if not has_movable_pit(raw_board[0:2]):
            return False, "Game over. None of opponent's pits has at least 2 seeds. Player won."

    return True, ''


def print_start_layout(start_board, args):
    print("""
Input CSV : {args.csv_filepath}
Output CSV: {args.outcsv_filepath}

Board layout before the turn:
""".format(version=VERSION, args=args))

    utils.print_board(start_board, INDENT)
    print('\nRunning algorithm "{}" ...'.format(args.algo))


def print_result_layout(result):
    print('\nBoard layout after the turn:\n')
    utils.print_board(result.board.to_raw_board(), INDENT)
    print("""
Optimal starting pit: {result.pit_id}
Captured seeds      : {result.captured_seeds}
""".format(result=result))


def load_algo(algo_name):
    """
    Loads the algorithm with the given name and returns the algorithm’s execution function.
    Raises ImportError if loading the algorithm’s module or execution function fails.
    :rtype: algoutils.InternalBoard -> algoutils.AlgoResult
    """
    algo_internal_name = 'algo_' + algo_name

    try:
        algo_module = importlib.import_module(algo_internal_name)
        algo_func = getattr(algo_module, algo_internal_name)

        if not callable(algo_func):
            raise AttributeError('{0}.{0} is not callable.'.format(algo_internal_name))
    except (ImportError, AttributeError) as err:
        raise ImportError('Could not load algorithm {}.\n{}'.format(algo_name, err))

    return algo_func


def parse_cli():
    parser = argparse.ArgumentParser(
        description=
            'Given a Bao board determines the optimal pit to start a turn, i.e. the pit '
            'that leads to the maximum possible number of captured seeds.'
        , epilog=
            'See the documentation (Doku.rst) for more details.'
    )

    parser.add_argument('csv_filepath', metavar='<input CSV file>',
                        help='Path to the CSV file with the starting board layout.')

    parser.add_argument('-o', '--output', metavar='<output CSV file>', dest='outcsv_filepath',
                        default=None,
                        help='Path to the output CSV file for the board configuration '
                             'after the turn. (Default: stdout)')

    parser.add_argument('-a', '--algo', metavar='<algorithm name>', dest='algo', default='bruteforce',
                        help='Specifies the algorithm to use. (Default: %(default)s)')

    parser.add_argument('-d', '--debug', dest='is_debug', action='store_true', default=False,
                        help='Enables debug mode. (Default: %(default)s)')

    return parser.parse_args()


if __name__ == '__main__':
    sys.exit(0 if main() else 1)
